import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;


public class ClientConnection extends Thread implements IConnection
{
	private Socket            socket;
	private ConnectionHandler ch;
	private BufferedReader    InClient;
	private PrintWriter       OutClient;
	
	public ClientConnection(Socket socket, ConnectionHandler ch)
	{
		this.socket = socket;
		this.ch     = ch;
		
		ch.addClient();
		
		try{
        // reads income calls from client
        InClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    	
        // creates output stream to client
        OutClient = new PrintWriter(this.socket.getOutputStream(), true);
        
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
    	System.out.println("Client Object Created.");
		
		new Thread(this).start();	
	}
	
	public void run()
	{
		String message ="";
		try 
		{   
	        while ((message = InClient.readLine()) != null){
	        	ch.Communicate(message);
	        	System.out.println("Recieved from ClientConnection :" + message);
	        	message = "";
	        } 
		} 
		catch (IOException e){
			System.out.println("Lost client connection.");
			//e.printStackTrace();			
		}
		ch.removeClient(this);
	}
	
	public void sendMessage (String message)
	{
		try {
        	OutClient.println(message);
        	System.out.println("From SendMessage a message has been sent");
		}
		catch (Exception e)
		{
        	System.out.println("From SendMessage : this client is being removed.");
			ch.removeClient(this);
		}
	}
	
	public void kill ()
	{
		try{
		socket.close();
		InClient.close();
		OutClient.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
