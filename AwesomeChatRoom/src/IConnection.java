
public interface IConnection {
	void kill ();
	void run ();
	void sendMessage (String message);
}
