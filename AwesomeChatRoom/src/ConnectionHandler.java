import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ConnectionHandler extends Thread implements IConnectionHandler{
	private ServerSocket socket = null;
	private int clientCount = 0;
	private int connectionLimit = 0;
	private List<IConnection> clientConnections;
	private volatile boolean finished = false;
	
	public ConnectionHandler (ServerSocket socket, int threadCount)
	{
		System.out.println("Connection Handler created.");
		this.connectionLimit = threadCount;
		this.socket = socket;
		clientConnections = new ArrayList<IConnection>();
		this.start();
	}
	
	public void stopThread()
	{
		this.finished = true;
	}
	
	@Override
	public void run() 
	{
		while (!finished)
		{
			if(clientCount < connectionLimit){
				Socket client = null;
				try {
					client = socket.accept();
					System.out.println("Client Connected!");
					clientConnections.add(new ClientConnection(client, this));
				}
				catch (Exception e)
				{
					//e.printStackTrace();
				}
			}
		}
	}
	
	public synchronized void Communicate(String msg)
	{
		System.out.println("Message :" + msg);
		for(int i = 0; i < clientConnections.size(); i++)
		{
			clientConnections.get(i).sendMessage(msg);
		}	
	}
	
	public synchronized void removeClient (IConnection client)
	{
		System.out.println("Disconnecting :");
		try
		{
			client.kill();
			System.out.println("called kill on client");
            for(int i=0;i<clientConnections.size();i++) {
            	System.out.println("looking for client");
                if((clientConnections.get(i)).equals(client)){
                	System.out.println("removing client from list.");
                	clientConnections.remove(i);
                	clientCount--;
                }
            }
            System.out.println("Client count after removal equals :"+ clientConnections.size());
            
		}
		catch(Exception e)
		{}
		System.out.println("Client Disconnected :");
	}
	
	public synchronized void addClient ()
	{
		clientCount++;
		System.out.println("["+this.socket.getLocalPort()+"] Client count equals :"+ clientCount);
	}

	@Override
	public int getPort() {
		return this.socket.getLocalPort();
		
	}
	
	@Override
	public int getMaxThread() { 
		return this.connectionLimit;
	}

	@Override
	public void setMaxThread(int threadCount) {
		this.connectionLimit = threadCount;
		
	}
	
	public void removeAllClients()
	{
		try{
		Iterator<IConnection> i = clientConnections.iterator();
		
		while(i.hasNext())
		{
			IConnection c = (IConnection)i.next();
			c.kill();
			i.remove();
		}
		socket.close();
		}
		catch (IOException e)
		{
			System.out.println("Exception thrown while trying to remove all clients");
		}
	}
	
}
