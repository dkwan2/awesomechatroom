import java.io.IOException;
import java.io.InputStream;

public class TimeoutInputStream extends InputStream {
	private InputStream in;
	private int timeout;

	public TimeoutInputStream(InputStream i, int t) {
		in = i;
		timeout = t;
	}

	public int read() throws IOException {
		long start_time = System.currentTimeMillis();
		while (in.available() < 1) {
			long current_time = System.currentTimeMillis();
			if ((current_time - start_time) > timeout) {
				throw new IOException("Timeout");
			}
		}

		return in.read();
	}
}