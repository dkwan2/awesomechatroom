import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

public class AdminApplet extends Applet {
	private TextField location;
	private TextField numberOfThreads;
	private TextField port;
	//private JCheckBox newChat;
	private Label label1 = new Label("Server IP address :");
	private Label label2 = new Label("Maximum connections allowed :");
	private Label label3 = new Label("Port number :");
	private Label label5 = new Label("");
	private JPanel control_panel = new JPanel(new GridLayout(4, 2));
	private JPanel bottom_panel = new JPanel(new GridLayout(3, 1));
	private JPanel table_holder = new JPanel(new BorderLayout());
	private JPanel center_panel = new JPanel(new BorderLayout());
	private JPanel west_panel = new JPanel(new BorderLayout());
	private JPanel radio_holder = new JPanel(new GridLayout(1,3));
	private String serverLocation = null;

	public void init() {
		this.setSize(950, 200);
		this.setBackground(Color.lightGray);
		this.setLayout(new BorderLayout());
		
		// Set message label to red
		this.label5.setForeground(Color.red);
		
		// Construct check box
		//this.newChat = new JCheckBox();
		
		// Construct submit button
		final Button b = new Button("Submit");
		b.setEnabled(false);
		
		// Construct table
		final DefaultTableModel t = new DefaultTableModel();
		t.addColumn("Port");
		t.addColumn("Connection Limit");
		JTable table = new JTable(t);
		JScrollPane scrollPane = new JScrollPane(table);
		
		// Table row listener
		final ListSelectionModel m = table.getSelectionModel();  
        m.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);  
        m.addListSelectionListener(new ListSelectionListener() {
        	public void valueChanged(ListSelectionEvent e) {
        		if (e.getValueIsAdjusting()) {
        			int row = m.getLeadSelectionIndex(); 
        			String p = (String) t.getValueAt(row, 0);
        			String th = (String) t.getValueAt(row, 1);
        			location.setText(serverLocation);
        			port.setText(p);
        			numberOfThreads.setText(th);
        			sanityCheck(b);
        		}
            }
        });

		// Construct the TextFields
		this.location = new TextField(20);
		this.location.addKeyListener(new KeyListener() {
			@Override
			public void keyReleased(KeyEvent e) {
				sanityCheck(b);
			}
			@Override
			public void keyPressed(KeyEvent e) {}
			@Override
			public void keyTyped(KeyEvent e) {}
		});
		
		this.numberOfThreads = new TextField(20);
		this.numberOfThreads.addKeyListener(new KeyListener() {
			@Override
			public void keyReleased(KeyEvent e) {
				sanityCheck(b);
			}
			@Override
			public void keyPressed(KeyEvent e) {}
			@Override
			public void keyTyped(KeyEvent e) {}
		});
		
		this.port = new TextField(20);
		this.port.addKeyListener(new KeyListener() {
			@Override
			public void keyReleased(KeyEvent e) {
				sanityCheck(b);
			}
			@Override
			public void keyPressed(KeyEvent e) {}
			@Override
			public void keyTyped(KeyEvent e) {}
		});
		
		// Construct radio buttons
		JRadioButton radio_new = new JRadioButton("Create new chat room");
		JRadioButton radio_existing = new JRadioButton("Edit existing chat room");
		JRadioButton radio_shutdown = new JRadioButton("Shutdown chat room");
		ButtonGroup radio_group = new ButtonGroup();
		radio_group.add(radio_new);
		radio_group.add(radio_existing);
		radio_group.add(radio_shutdown);
		radio_new.setSelected(true);

		// add components to the layout
		control_panel.add(new Label("Chat Room Information"));
		control_panel.add(new Label(""));
		control_panel.add(label1);
		control_panel.add(location);
		control_panel.add(label3);
		control_panel.add(port);
		control_panel.add(label2);
		control_panel.add(numberOfThreads);
		
		table_holder.add(new Label("Select Existing Chat Room:"), BorderLayout.NORTH);
		table_holder.add(scrollPane, BorderLayout.CENTER);
		
		bottom_panel.add(b);
		bottom_panel.add(new Label("Messages:"));
		bottom_panel.add(label5);
		
		radio_holder.add(radio_new, BorderLayout.WEST);
		radio_holder.add(radio_existing, BorderLayout.CENTER);
		radio_holder.add(radio_shutdown, BorderLayout.EAST);
		
		west_panel.add(control_panel, BorderLayout.NORTH);
		west_panel.add(radio_holder, BorderLayout.SOUTH);

		center_panel.add(west_panel, BorderLayout.WEST);
		center_panel.add(table_holder, BorderLayout.EAST);
		
		this.add(center_panel, BorderLayout.CENTER);
		this.add(bottom_panel, BorderLayout.SOUTH);
		
		// specify that action events sent by the
		// button or the input TextField should be handled
		// by the same CapitalizerAction object
		SubmitListener sl = new SubmitListener(location, numberOfThreads, port, radio_group, label5);
		b.addActionListener(sl);

		// notice that ActionEvents produced by output are ignored.
		
		Timer timer = new Timer("updateAdmin");
		Update update = new Update(this, t, label5);
		timer.schedule(update, 0, 5000);
	}
	
	public void setServerLocation(String s) {
		this.serverLocation = s;
	}
	
	public String getServerLocation() {
		return this.serverLocation;
	}
	
	public void sanityCheck(Button b) {
		boolean isEnabled = true;		
		if (location.getText().length() > 0) {
			if ((!location.getText().equals("localhost"))
					&& (!location.getText().matches("[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}"))) {
				isEnabled = false;
			} else {
				setServerLocation(location.getText());
			}
		} else {
			isEnabled = false;
		}
		if (numberOfThreads.getText().length() > 0) {
			try {
				Integer.parseInt(numberOfThreads.getText());
			} catch (NumberFormatException ex) {
				isEnabled = false;
			}
		} else {
			isEnabled = false;
		}
		if (port.getText().length() > 0) {
			try {
				Integer.parseInt(port.getText());
			} catch (NumberFormatException ex) {
				isEnabled = false;
			}
		} else {
			isEnabled = false;
		}
		b.setEnabled(isEnabled);
	}

	class SubmitListener implements ActionListener {

		private TextField location;
		private TextField numberOfThreads;
		private TextField port;
		private ButtonGroup radio_group;
		private Label label5;

		public SubmitListener(TextField location, TextField numberOfThreads, TextField port, ButtonGroup radio_group, Label label5) {
			this.location = location;
			this.numberOfThreads = numberOfThreads;
			this.port = port;
			this.radio_group = radio_group;
			this.label5 = label5;
		}
		
		public JRadioButton getSelected(ButtonGroup group) {
			for (Enumeration e = group.getElements(); e.hasMoreElements(); ) {
			   JRadioButton b = (JRadioButton)e.nextElement();
			   if (b.getModel() == group.getSelection()) {
			       return b;
			   }
			}
			return null;
		}

		public void actionPerformed(ActionEvent ae) {
			String serverLocation = location.getText();
			String numThreads = numberOfThreads.getText();
			String port = this.port.getText();
			Socket clientSocket;
			JRadioButton selected_radio;
			int action = 0;
			
			if ((selected_radio = getSelected(radio_group)) != null) {
				if (selected_radio.getText().matches(".*[C|c]reate.*")) {
					action = 1;
				}
				if (selected_radio.getText().matches(".*[E|e]dit.*")) {
					action = 2;
				}
				if (selected_radio.getText().matches(".*[S|s]hutdown.*")) {
					action = 3;
				}
			}

			try {
				clientSocket = new Socket(serverLocation, 80);
				PrintWriter OutServer = new PrintWriter(clientSocket.getOutputStream(), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

				OutServer.println(numThreads + "-" + port + "-" + action);
				String message = in.readLine();
				if (message.equals("Success!")) {
					location.setText("");
					numberOfThreads.setText("");
					this.port.setText("");
				}
				label5.setText(message);

				clientSocket.close();
				OutServer.close();
				in.close();
			} catch (UnknownHostException e) {
				label5.setText("Server not responding");
				//e.printStackTrace();
			} catch (IOException e) {
				label5.setText("Server not responding");
				//e.printStackTrace();
			}
		}
	}
	
	class Update extends TimerTask {
		
		private AdminApplet adminApplet;
		private PrintWriter out;
		private BufferedReader in;
		private Socket socket;
		private DefaultTableModel table;
		private Label error;
		
		public Update(AdminApplet app, DefaultTableModel t, Label l) {
			adminApplet = app;
			table = t;
			error = l;
		}

		@Override
		public void run() {			
			if (adminApplet.getServerLocation() != null) {
				String input = "";
				try {
					socket = new Socket(adminApplet.getServerLocation(), 80);
					out = new PrintWriter(socket.getOutputStream(), true);
					in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
					out.println("-updateAdmin-");
					input = in.readLine();
					socket.close();
					error.setText("");
				} catch (UnknownHostException e) {
					error.setText("Server not responding");
					//e.printStackTrace();
				} catch (IOException e) {
					error.setText("Server not responding");
					//e.printStackTrace();
				}				
				
				if (input.length() > 0) {
					List <String> list = Arrays.asList(input.split(" "));
					Iterator<String> iterator = list.iterator();
					
					table.getDataVector().removeAllElements();	
					while (iterator.hasNext()) {
						String port = iterator.next();
						String threads = null;					
						if (iterator.hasNext()) {
							threads = iterator.next();
						}
						
					  	Vector <String> row = new Vector<String>();  
						row.addElement(port);
						row.addElement(threads);
						table.addRow(row);
					}				
				}
			}
		}	
	}

}
