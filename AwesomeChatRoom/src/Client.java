import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Client {
	
	private String username = null;
	private String host = null;
	private int port = 0;
	private static JLabel status;
	
	public Client() { }
	
	public void setUsername (String u) {
		username = u;
	}
	
	public void setHost(String h) {
		host = h;
	}
	
	public void setPort(int p) {
		port = p;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getHost() {
		return host;
	}
	
	public int getPort() {
		return port;
	}
	
	public boolean sanityCheck(Client c) {
		if ((c.getUsername() != null) && (c.getHost() != null) && (c.getPort() != 0)) {
			return true;
		}
		return false;
	}
	
	public static Socket getSocket(Client c) {
		Socket socket = null;
		try {
			socket = new Socket(c.getHost(), c.getPort());
			PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
			out.println(" " + c.getUsername() + " has joined the chat room...");
			TimeoutInputStream in = new TimeoutInputStream(socket.getInputStream(), 1000);
			if (in.read() > 0) {
				status.setText("Status: Connected");
				return socket;
			}
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				socket = null;
			}
			status.setText("Status: Server is busy");
			return socket;
		} catch (UnknownHostException e) {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException ex) {
					e.printStackTrace();
				}
				socket = null;
			}
			status.setText("Status: Could not connect to server");
			return socket;
			//e.printStackTrace();
		} catch (IOException e) {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException ex) {
					e.printStackTrace();
				}
				socket = null;
			}
			status.setText("Status: Could not connect to server");
			return socket;
			//e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		
		final Client client = new Client();
		
		JFrame frame = new JFrame("Client Application");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel(new BorderLayout());
		JPanel control_panel = new JPanel(new GridLayout(3,2));
		JPanel bottom_panel = new JPanel(new GridLayout(2,1));
		
		final JButton button = new JButton();
		button.setEnabled(false);
		button.setText("Connect");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Socket socket = null;
				if ((socket = getSocket(client)) != null) {
					new Thread(new ChatRoom(client.getUsername(), socket)).start();
				}
			}
		}); 	
		
		final JTextField username_input = new JTextField(20);
		username_input.addKeyListener(new KeyListener() {
	    	@Override
			public void keyReleased(KeyEvent e) {
	    		if (username_input.getText().length() > 0) {
	    			String value = username_input.getText();
	    			client.setUsername(value);
	    			if (client.sanityCheck(client)) {
	    				button.setEnabled(true);
	    			}
	    		} else {
    				button.setEnabled(false);
    			}
	    	}
	    	@Override
			public void keyPressed(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
	    });
	    
	    final JTextField host_input = new JTextField(20);
	    host_input.addKeyListener(new KeyListener() {
	    	@Override
			public void keyReleased(KeyEvent e) {
	    		if (host_input.getText().length() > 0) {
	    			String value = host_input.getText();
	    			if ((value.equals("localhost")) || (value.matches("[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}"))) {
	    				client.setHost(value);
	    				if (client.sanityCheck(client)) {
	    	    			button.setEnabled(true);
	    	    		}
	    			} else {
	    				button.setEnabled(false);
	    			}
	    		}
	    	}
	    	@Override
			public void keyPressed(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
	    });
	    
	    final JTextField port_input = new JTextField(20);
	    port_input.addKeyListener(new KeyListener() {
			@Override
			public void keyReleased(KeyEvent e) {
				try {
	    			int value = Integer.parseInt(port_input.getText());
	    			client.setPort(value);
	    			if (client.sanityCheck(client)) {
		    			button.setEnabled(true);
		    		}
	    		} catch(NumberFormatException ex) {
	    			button.setEnabled(false);
	    		}
			}
			@Override
			public void keyPressed(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
	    });
	    
	    status = new JLabel("Status:");
		
		frame.setContentPane(panel);
		panel.add(new JLabel("Awesome Chat Room"), BorderLayout.NORTH);
		panel.add(control_panel, BorderLayout.CENTER);
		control_panel.add(new JLabel("Username:"));
		control_panel.add(username_input);
		control_panel.add(new JLabel("Host:"));
		control_panel.add(host_input);
		control_panel.add(new JLabel("Port:"));
		control_panel.add(port_input);
		panel.add(bottom_panel, BorderLayout.SOUTH);
		bottom_panel.add(button);
		bottom_panel.add(status);
		
		frame.setVisible(true);
		frame.pack();
	}
}
