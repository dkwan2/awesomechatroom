import java.text.DateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class AwesomeFormatter extends Formatter {
	
	@Override
	public String format(LogRecord record) {
		
		String output = "";
		DateFormat date = DateFormat.getDateTimeInstance();
		
		output += "[ ";
		output += date.format(new Date(record.getMillis()));
		output += " ";
		output += record.getLevel();
		output += " ] ";
		output += formatMessage(record);
		output += System.getProperty("line.separator");
		
		return output;
	}
}
