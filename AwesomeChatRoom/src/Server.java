import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

public class Server
{	
	List<ConnectionHandler> Connections;
	public Server() 
	{ 
		Connections = new ArrayList ();
	}

	public boolean start(int port , int threadCount) {
		if(!doesClientHandlerExists(port)){
			ServerSocket server = null;
			try{
				server = new ServerSocket(port);
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}

			clientHandler(server, threadCount);
			return true;
		}
		return false;
	}
	
	public void adminStart() {

		ServerSocket adminServer = null;
	    try{
	    	adminServer = new ServerSocket(80);
	    }
	    catch(IOException e)
	    {
	    	e.printStackTrace();
	    }

		serverHandler(adminServer);
	}
	
	private void clientHandler(ServerSocket serverSocket , int threadCount)
	{
		ConnectionHandler ch = new ConnectionHandler(serverSocket, threadCount);
		Connections.add(ch);
	}
	
	private void serverHandler(ServerSocket adminServer)
	{
		    new AdminConnectionHandler(adminServer, this).start();
	}
	
	public Boolean setMaxThreads (int port , int threads)
	{
		for(ConnectionHandler ch : Connections)
		{
			if (ch.getPort() == port)
			{
				ch.setMaxThread(threads);
				return true;
			}
		}
		return false;
	}
	public Boolean doesClientHandlerExists (int port)
	{
		for(ConnectionHandler ch : Connections)
		{
			if (ch.getPort() == port)
			{
				return true;
			}
		}
		return false;
	}
	public Boolean removeClientHandler (int port)
	{
		for(ConnectionHandler ch : Connections)
		{
		    System.out.println("Looking..........");
			if (ch.getPort() == port)
			{
				System.out.println("Connection matched.");
				ch.removeAllClients();
				System.out.println("Removed all connections");
				ch.stopThread();
				System.out.println("thread stopped");
				Connections.remove(ch);
				System.out.println("Connection found and Removed.");
				return true;
			}
		}
		System.out.println("Connection not found.");
		return false;
	}
	
	public List<ConnectionHandler> getConnectionHandlers() {
		return Connections;
	}
	
}
