import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;


public class AdminConnection extends Thread implements IConnection {
	private Socket            socket;
	private AdminConnectionHandler ch;
	private BufferedReader    InClient;
	private PrintWriter       OutClient;
	
	public AdminConnection(Socket socket, IConnectionHandler ch)
	{
		this.socket = socket;
		this.ch     = (AdminConnectionHandler)ch;
		
		ch.addClient();
		
		try{
        // reads income calls from client
        InClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    	
        // creates output stream to client
        OutClient = new PrintWriter(this.socket.getOutputStream(), true);
        
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
		this.start();	
	}
	
	public void run()
	{
		String message ="";
		try 
		{   
	        while ((message = InClient.readLine()) != null){
	        	//System.out.println("Recieved from ClientConnection :" + message);
	        	
	        	if (message.equals("-updateAdmin-")) {
	        		String connections_info = "";
	        		List<ConnectionHandler> connections = ch.getConnectionHandlers();
	        		for (ConnectionHandler c : connections) {
	        			connections_info += Integer.toString(c.getPort()) + " ";
	        			connections_info += Integer.toString(c.getMaxThread()) + " ";
	        		}
	        		OutClient.println(connections_info);
	        	} else {	        	
	        		switch(ch.handleRequest(message)) {
	        		case 0:
	        			OutClient.println("Success!");
	        			break;
	        		case 1:
	        			OutClient.println("Failed to set connection limit");
	        			break;
	        		case 2:
	        			OutClient.println("Port number and connection limit must be greater than zero");
	        			break;
	        		case 3:
	        			OutClient.println("Error. Chat room does not exist. ");
	        			break;
	        		default:
	        			OutClient.println("Error!");
	        			break;
	        		}	        	
	        	}
	        } 
		} 
		catch (IOException e){
			System.out.println("client loop throwing exception.");
			e.printStackTrace();			
		}
		
		ch.removeClient(this);
	}
	
	public void kill ()
	{
    //	System.out.println("From Kill this Client is being killed.");
		try{
		InClient.close();
		OutClient.close();
		socket.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void sendMessage(String message) {}
	
}
