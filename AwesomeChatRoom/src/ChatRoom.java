import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ChatRoom implements Runnable {

	private String username;
	private Socket client;
	private PrintWriter out;
	private BufferedReader in;
	private String received = "";
	private String sent = "Connected to server...";
	private JTextArea message_display;
	private JTextArea message_input;

	public ChatRoom(String u, Socket c) {
		username = u;
		client = c;
	}

	public void run() {

		buildGUI();

		while (true) {
			try {
				in = new BufferedReader(new InputStreamReader(client.getInputStream()));
				String input = in.readLine();
				if (input != null) { 
					received += input + "\n";
					message_display.setText(received);
				} else {
					shutdown();
					System.exit(0);
				}
			} catch (IOException e) {
				// e.printStackTrace();
			}
		}
	}

	public void buildGUI() {
		JFrame frame = new JFrame("Chat Room");
		frame.setSize(400, 400);
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				shutdown();
				System.exit(0);
			}

			public void windowOpened(WindowEvent e) {
				message_input.requestFocus();
			}
		});

		JMenuBar menubar = new JMenuBar();
		JMenu menu = new JMenu("Options");
		JMenuItem m = new JMenuItem("Disconnect");
		m.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				shutdown();
				System.exit(0);
			}
		});

		menu.add(m);
		menubar.add(menu);
		frame.setJMenuBar(menubar);

		JPanel panel = new JPanel(new BorderLayout());
		JPanel control_panel = new JPanel(new BorderLayout());

		message_display = new JTextArea();
		message_display.setLineWrap(true);
		message_display.setAutoscrolls(true);
		message_display.setEditable(false);
		message_display.setBorder(BorderFactory
				.createLineBorder(Color.black, 1));

		JScrollPane scroll_display = new JScrollPane(message_display);

		message_input = new JTextArea();
		message_input.setLineWrap(true);
		message_input.setAutoscrolls(true);
		message_input.setBorder(BorderFactory.createLineBorder(Color.black, 1));
		message_input.addKeyListener(new KeyListener() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					sendMessage();
					message_input.setText("");
				}
			}

			@Override
			public void keyPressed(KeyEvent arg0) {
			}

			@Override
			public void keyTyped(KeyEvent arg0) {
			}
		});

		JScrollPane scroll_input = new JScrollPane(message_input);

		JButton button = new JButton("Send");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sendMessage();
				message_input.setText("");
			}
		});

		panel.add(scroll_display, BorderLayout.CENTER);
		panel.add(control_panel, BorderLayout.SOUTH);
		control_panel.add(scroll_input, BorderLayout.CENTER);
		control_panel.add(button, BorderLayout.EAST);

		frame.setContentPane(panel);
		frame.setVisible(true);
	}

	public void sendMessage() {
		try {
			sent = "[" + username + "] " + message_input.getText();
			this.out = new PrintWriter(client.getOutputStream(), true);
			this.out.println(sent);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void shutdown() {
		try {
			client.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
