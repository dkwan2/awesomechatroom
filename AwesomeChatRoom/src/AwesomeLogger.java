import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AwesomeLogger {

	private final static Logger logger = Logger.getLogger(AwesomeLogger.class.getName());
	private static Handler handler = null;

	public synchronized static void startLogger(boolean debug) {
		
		logger.setLevel(Level.SEVERE);	
		if (debug) {
			logger.setLevel(Level.INFO);
		}
		
		// Redirect System.err.println and Exceptions to logger
		System.setErr(new PrintStream(System.err) {
			public void print(String s) {
				setup();
				logger.severe(s);
				teardown();
				//AwesomeEmailer.send("hellosmtpworld@hotmail.com", "hellosmtpworld@hotmail.com", "PANIC!!!", s);
			}
		});

		// Redirect System.out.println to logger
		System.setOut(new PrintStream(System.out) {
			public void print(String s) {
				setup();
				logger.info(s);
				teardown();
			}
		});
	}
	
	private static void setup() {
		String filename = "server.log";
		File file = new File(filename);
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		try {
			handler = new FileHandler(filename, true);
			handler.setFormatter(new AwesomeFormatter());
			logger.addHandler(handler);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void teardown() {
		handler.flush();
		handler.close();
	}
}
