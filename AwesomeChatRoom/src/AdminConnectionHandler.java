import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class AdminConnectionHandler extends Thread implements IConnectionHandler {

	private ServerSocket socket = null;
	private Server server;
	private int clientCount = 0;
	private int connectionLimit = 0;
	private List<IConnection> clientConnections;
	
	public AdminConnectionHandler (ServerSocket socket, Server server)
	{
		this.server = server;
		this.socket = socket;
		clientConnections = new ArrayList<IConnection>();
		//this.run();
	}
	
	@Override
	public void run() {
		while (true)
		{
			Socket client = null;
			try {
				client = socket.accept();
				//System.out.println("Client Connected!");
				clientConnections.add(new AdminConnection(client, this));
			}
			catch (Exception e)
			{
				//e.printStackTrace();
			}
		}		
	}

	@Override
	public void removeClient(IConnection client) {
	//	System.out.println("Disconnecting :");
		try
		{
			client.kill();
            for(int i=0;i<clientConnections.size();i++) {
                if((clientConnections.get(i)).equals(client)){
                	clientConnections.remove(i);
                	clientCount--;
                }
            }
          //  System.out.println("Client count equals :"+ clientCount);
		}
		catch(Exception e)
		{}
		//System.out.println("Disconnected :");
	}

	@Override
	public void addClient() {
		clientCount++;
	//	System.out.println("Client count equals :"+ clientCount);
	}

	@Override
	public int getPort() {
		return this.socket.getLocalPort();
		
	}
	
	@Override
	public int getMaxThread() {
		return this.connectionLimit;
	}

	@Override
	public void setMaxThread(int threadCount) {
		this.connectionLimit = threadCount;
	}
	
	public int handleRequest (String message)
	{
		int rc = -1;
		if (message != null) {
			List<String> s = Arrays.asList(message.split("-"));
			int threads   = parseThreads(s.get(0));
			int port      = parsePort(s.get(1));
			int action = parseAction(s.get(2));
		
			if (threads != 0 && port != 0) {
				switch(action) {
				case 1: // new chat room
					System.out.println(String.valueOf(port) + String.valueOf(threads));
					server.start(port, threads);
					System.out.println("true server has been created");
					rc = 0;
					break;
				case 2: // edit existing chat room
					if (server.setMaxThreads(port, threads)) {
						System.out.println("Max threads updated to : " + threads + " on port: " + port);
						rc = 0;
					} else {
						System.out.println("Failed to set max threads");
						rc = 1;
					}
					break;
				case 3: // shutdown chat room
					System.out.println("Shutdown chat room");
					
					if(server.removeClientHandler(port))
						rc = 0; // Success
					else
						rc = 3; // Failed 
					
					break;
				default: // invalid action error
					System.out.println("Invalid action code in handleRequest: " + action);
					break;
				}
			} else {
				System.out.println("Thread count or port value is invalid");
				rc = 2;
			}
		}
		return rc;
	}

	public int parseThreads (String threads)
	{
		System.out.println("Threads: " + threads);
		int threadcount ;
		try{
		threadcount = Integer.parseInt(threads);
		}
		catch(NumberFormatException e)
		{
		threadcount = 0;
		}
		
		return threadcount;
	}
	public int parsePort (String port)
	{ 
		System.out.println("Port: " + port);
		int numport ;
		try{
		numport = Integer.parseInt(port);
		}
		catch(NumberFormatException e)
		{
		numport = 0;
		}
		
		return numport;
	}
	public int parseAction (String a)
	{
		System.out.println("Action: " + a);
		int action;
		try{
			action = Integer.parseInt(a);
		}
		catch(NumberFormatException e)
		{
			action = 0;
		}
		
		return action;
	}
	
	public List<ConnectionHandler> getConnectionHandlers() {
		return server.getConnectionHandlers();
	}
}
